using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using OnlineTesting.Data;
using OnlineTesting.Interfaces;
using OnlineTesting.Models;
using OnlineTesting.Models.AnswersViewModels;
using OnlineTesting.Models.TestViewModels;

namespace OnlineTesting.Controllers
{
    public class AnswersController : BasicController
    {
        private readonly IAnswersService _answersService;

        public AnswersController(IAnswersService answersService,
            UserManager<ApplicationUser> userManager
        ) : base(userManager)
        {
            _answersService = answersService;
        }

        public async Task<IActionResult> Index(string testId, string questionId, bool? showErrors)
        {
            var answers =
                _answersService.GetAnswrsFor(testId, questionId, await GetCurrentUser());

            if (answers == null)
            {
                return RedirectToAction("Timeout", "Error");
            }

            if (showErrors ?? false)
            {
                var errors =
                    _answersService.GetModelStateErrors(testId, questionId, await GetCurrentUser());

                foreach (var error in errors)
                {
                    ModelState.AddModelError("", error);
                }
            }

            return View(new AnswersIndexVM() {QuestionId = questionId, Answers = answers, TestId = testId});
        }

        public IActionResult Create(string questionId, string testId)
        {
            var answer = new AnswerVM() {QuestionId = questionId, TestId = testId};
            return View("CreateEdit", answer);
        }

        [HttpPost]
        public async Task<IActionResult> Save(AnswerVM answer)
        {
            if (ModelState.IsValid)
            {
                _answersService.SaveAnswer(answer, await GetCurrentUser());
                return RedirectToAction("Index", new {questionId = answer.QuestionId, testId = answer.TestId});
            }

            return View("CreateEdit", answer);
        }

        public async Task<IActionResult> Edit(string id, string questionId, string testId)
        {
            var answer =
                _answersService.GetAnswer(id, questionId, testId, await GetCurrentUser());
            return View("CreateEdit", answer);
        }

        public async Task<IActionResult> Delete(string id, string questionid, string testId)
        {
            _answersService.RemoveAnswer(id, questionid, testId, await GetCurrentUser());

            return RedirectToAction("Index", new {questionId = questionid, testId});
        }

        public async Task<IActionResult> Submit(string questionId, string testId)
        {
            if (_answersService.CheckQuestionStateIsValid(questionId, testId, await GetCurrentUser())
            )
            {
                return RedirectToAction("Index", "Questions", new {testId});
            }
            return RedirectToAction("Index", new {questionId, testId, showErrors = true});
        }
    }
}