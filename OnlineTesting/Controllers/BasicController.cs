using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using OnlineTesting.Data;
using OnlineTesting.Models;

namespace OnlineTesting.Controllers
{
    public abstract class BasicController : Controller
    {
        
        
        private readonly UserManager<ApplicationUser> _userManager;

        private ApplicationUser _currentUser;
        
        protected async Task<ApplicationUser> GetCurrentUser()
        {
            return _currentUser = _currentUser ?? await _userManager.GetUserAsync(HttpContext.User);
        }

        protected BasicController(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }
    }
}