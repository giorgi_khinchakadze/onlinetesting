﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using OnlineTesting.Data;
using OnlineTesting.Interfaces;
using OnlineTesting.Models.HomeViewModels;

namespace OnlineTesting.Controllers
{
    [Authorize]
    public class HomeController : BasicController
    {
        private readonly IHomeService _homeService;

        public HomeController(IHomeService homeService,
            UserManager<ApplicationUser> userManager
        ) : base(userManager)
        {
            _homeService = homeService;
        }

        [AllowAnonymous]
        public IActionResult Index()
        {
            return View(new SearchVM() {Tests = _homeService.GetAvailableTests()});
        }

        [HttpPost]
        public IActionResult Index(SearchVM searchParams)
        {
            searchParams.Tests =
                _homeService.GetAvailableTests(searchParams.Name, searchParams.CategoryId, searchParams.Difficulty);
            return View(searchParams);
        }

        public IActionResult Start(string id)
        {
            return View(_homeService.GetTest(id));
        }

        public async Task<IActionResult> Results()
        {
            return View(_homeService.GetResultsFor(await GetCurrentUser()));
        }

        [AllowAnonymous]
        public IActionResult Ratings(string testId)
        {
            var ratings = _homeService.GetRatingsFor(testId);

            return View(new RatingsVM() {Ratings = ratings});
        }
    }
}