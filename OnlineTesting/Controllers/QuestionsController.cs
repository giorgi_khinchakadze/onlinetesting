using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using OnlineTesting.Data;
using OnlineTesting.Interfaces;
using OnlineTesting.Models;
using OnlineTesting.Models.QuestionViewModels;
using OnlineTesting.Models.TestViewModels;
using Remotion.Linq.Clauses.ResultOperators;

namespace OnlineTesting.Controllers
{
    public class QuestionsController : BasicController
    {
        private readonly IQuestionsService _questionsService;

        public QuestionsController(
            UserManager<ApplicationUser> userManager,
            IQuestionsService questionsService
        ) : base(userManager)
        {
            _questionsService = questionsService;
        }


        public async Task<IActionResult> Index(string testId, bool? showErrors)
        {
            var questions =
                _questionsService.GetCurrnentQuestions(testId, await GetCurrentUser());
            if (questions == null)
            {
                return RedirectToAction("Timeout", "Error");
            }

            if (showErrors ?? false)
            {
                var errors =
                    _questionsService.GetQuestionErrors(testId, await GetCurrentUser());
                foreach (var error in errors)
                {
                    ModelState.AddModelError(error.MemberNames.FirstOrDefault() ?? "", error.ErrorMessage);
                }
            }

            return View(new QuestionsIndexVM() {Questions = questions, TestId = testId});
        }


        public IActionResult Create(string testId)
        {
            return View("CreateEdit", new QuestionVM() {TestId = testId});
        }

        [HttpPost]
        public async Task<IActionResult> Save(QuestionVM question)
        {
            if (ModelState.IsValid)
            {
                if (_questionsService.AddOrUpdateQuestionOfCurrentTest(question, await GetCurrentUser()))
                {
                    return RedirectToAction("Index", "Answers",
                        new {questionId = question.Id, testId = question.TestId});
                }
                return RedirectToAction("Timeout", "Error");
            }
            return View("CreateEdit", question);
        }

        public async Task<IActionResult> Delete(string id, string testId)
        {
            _questionsService.RemoveQuestion(testId, id, await GetCurrentUser());
            return RedirectToAction("Index", new {testId});
        }

        public async Task<IActionResult> Edit(string id, string testId)
        {
            var question = _questionsService.GetQuestion(testId, id, await GetCurrentUser());
            if (question == null)
            {
                return RedirectToAction("Timeout", "Error");
            }
            return View("CreateEdit", question);
        }

        public async Task<IActionResult> Submit(string testId)
        {
            if (_questionsService.CheckQuestionsAreValid(testId, await GetCurrentUser()))
            {
                return RedirectToAction("Submit", "TestManagement", new {testId});
            }

            return RedirectToAction("Index", new {testId, showErrors = true});
        }
    }
}