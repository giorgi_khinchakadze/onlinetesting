using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.Host;
using OnlineTesting.Data;
using OnlineTesting.Interfaces;
using OnlineTesting.Models;
using OnlineTesting.Models.TestViewModels;

namespace OnlineTesting.Controllers
{
    [Authorize]
    public class TestManagementController : BasicController
    {
        private readonly ITestManagementService _testManagementService;


        public TestManagementController(
            UserManager<ApplicationUser> userManager,
            ITestManagementService managementService
        ) : base(userManager)
        {
            _testManagementService = managementService;
        }


        public IActionResult Index()
        {
            var tests = _testManagementService.GetTests();
            return View(tests);
        }


        public async Task<IActionResult> Create()
        {
            var test = new TestVM();
            _testManagementService.SetCurrentCachedTest(test, await GetCurrentUser());
            return RedirectToAction("Save", new {id = test.Id});
        }


        public async Task<IActionResult> Edit(string id)
        {
            var test = _testManagementService.GetTestFromDataBase(id);
            if (test == null)
            {
                return NotFound();
            }
            _testManagementService.SetCurrentCachedTest(test, await GetCurrentUser());
            return RedirectToAction("Save", new {id});
        }


        public async Task<IActionResult> Save(string id)
        {
            var test = _testManagementService.GetCurrnentTest(id, await GetCurrentUser());
            return View("CreateEdit", test);
        }

        [HttpPost]
        public async Task<IActionResult> Save(TestVM test)
        {
            if (ModelState.IsValid)
            {
                _testManagementService.AddOrUpdateCachedTest(test, await GetCurrentUser());

                return RedirectToAction("Index", "Questions", new {testId = test.Id});
            }

            return View("CreateEdit", test);
        }


        public async Task<IActionResult> Submit(string testId)
        {
            if (_testManagementService.CheckCurrentTestIsValid(testId, await GetCurrentUser()))
            {
                if (_testManagementService.SaveCurrentTestToDatabase(testId, await GetCurrentUser()))
                {
                    return RedirectToAction("Index");
                }
                return RedirectToAction("Timeout", "Error");
            }

            return RedirectToAction("Index", "Questions", new {testId = testId, showErrors = true});
        }

        public IActionResult Delete(string id)
        {
            _testManagementService.DeactivateTest(id);
            return RedirectToAction("Index");
        }


        public async Task<IActionResult> Cancel(string testId)
        {
            _testManagementService.CancelCurrentAction(testId, await GetCurrentUser());
            return RedirectToAction("Index");
        }
    }
}