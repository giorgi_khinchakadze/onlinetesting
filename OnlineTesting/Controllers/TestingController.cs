using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using OnlineTesting.Data;
using OnlineTesting.Interfaces;
using OnlineTesting.Models;
using OnlineTesting.Models.AnswersViewModels;
using OnlineTesting.Models.TestingViewModels;

namespace OnlineTesting.Controllers
{
    public class TestingController : BasicController
    {
        private readonly ITestingService _testingService;

        public TestingController(
            ITestingService testingService,
            UserManager<ApplicationUser> userManager
        ) : base(userManager)
        {
            _testingService = testingService;
        }


        [HttpPost]
        public async Task<IActionResult> Start(string id, bool isRated)
        {
            var resultId = _testingService.PrepareForTesting(id, isRated, await GetCurrentUser());
            return RedirectToAction("Next", new {resultId});
        }

        public async Task<IActionResult> Next(string resultId)
        {
            var question = _testingService.GetNextQuestion(await GetCurrentUser());

            if (question == null)
            {
                return RedirectToAction("Result", new {resultId});
            }

            return View(new NextVM() {Question = question, ResultId = resultId});
        }

        [HttpPost]
        public async Task<IActionResult> Next(List<String> Checked, string questionId, string resultId)
        {
            _testingService.AddAnswerResult(Checked, questionId, resultId, await GetCurrentUser());
            return RedirectToAction("Next", new {resultId});
        }

        public async Task<IActionResult> Result(string resultId)
        {
            var result = _testingService.GetResult(resultId ,await GetCurrentUser());
            if (result == null)
            {
                return RedirectToAction("Timeout", "Error");
            }
            return View(result);
        }
    }
}