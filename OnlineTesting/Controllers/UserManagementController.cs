using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using OnlineTesting.Data;
using OnlineTesting.Interfaces;
using OnlineTesting.Models;
using OnlineTesting.Models.UserViewModels;

namespace OnlineTesting.Controllers
{
    public class UserManagementController : BasicController
    {
        private readonly IUserManagementService _userManagementService;

        public UserManagementController(IUserManagementService userManagementService, UserManager<ApplicationUser> userManager) : base(userManager)
        {
            _userManagementService = userManagementService;
        }

        public IActionResult Index()
        {
            return View(_userManagementService.GetUserList());
        }

        public IActionResult Edit(string id)
        {
            var user = _userManagementService.GetUser(id);
            if (user == null)
            {
                return NotFound();
            }
            return View(user);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(UserVM user)
        {
            if (ModelState.IsValid)
            {
                var result = await _userManagementService.SaveUserEdit(user);
                return RedirectToAction("Index");
            }

            return View(user);
        }
    }
}