﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using OnlineTesting.Models.UserViewModels;

namespace OnlineTesting.Data
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {

    }
}
