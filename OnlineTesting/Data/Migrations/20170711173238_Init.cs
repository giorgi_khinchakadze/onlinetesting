﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OnlineTesting.Data
{
    public partial class UserFix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "Results",
                newName: "ApplicationUserId");

            migrationBuilder.AlterColumn<string>(
                name: "ApplicationUserId",
                table: "Results",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Results_ApplicationUserId",
                table: "Results",
                column: "ApplicationUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Results_AspNetUsers_ApplicationUserId",
                table: "Results",
                column: "ApplicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Results_AspNetUsers_ApplicationUserId",
                table: "Results");

            migrationBuilder.DropIndex(
                name: "IX_Results_ApplicationUserId",
                table: "Results");

            migrationBuilder.RenameColumn(
                name: "ApplicationUserId",
                table: "Results",
                newName: "UserId");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "Results",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
