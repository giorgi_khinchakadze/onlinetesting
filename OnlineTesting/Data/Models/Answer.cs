﻿using System.ComponentModel.DataAnnotations;
using AutoMapper;
using OnlineTesting.Attributes;

namespace OnlineTesting.Data.Models
{
    public class Answer : BasicEntity
    {
        [Required]
        public string Text { get; set; }

        [Required]
        public bool IsCorrect { get; set; }

        [Required]
        public string QuestionId { get; set; }

        [EFInclude]
        [IgnoreMap]
        public Question Question { get; set; }

        [Required]
        public string TestId { get; set; }
        
    }
}