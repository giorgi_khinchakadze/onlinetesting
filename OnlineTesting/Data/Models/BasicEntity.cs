﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OnlineTesting.Data.Models
{
    public abstract class BasicEntity
    {
        public BasicEntity()
        {
            Id = Guid.NewGuid().ToString();
        }

        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string Id { get; set; }

        [Required]
        public bool IsActive { get; set; }

        public DateTime? DateDeleted { get; set; }
        public DateTime DateCreated { get; set; }

    }

}