﻿using System.ComponentModel.DataAnnotations;

namespace OnlineTesting.Data.Models
{
    public class Category: BasicEntity
    {

        [Required]
        public string Name { get; set; }

    }
}
