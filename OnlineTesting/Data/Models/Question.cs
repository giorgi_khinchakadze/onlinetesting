﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AutoMapper;
using OnlineTesting.Attributes;

namespace OnlineTesting.Data.Models
{
    public class Question : BasicEntity
    {
        [Required]
        public decimal Time { get; set; }

        [Required]
        public decimal Score { get; set; }

        [Required]
        public string Text { get; set; }

        [Required]
        public string TestId { get; set; }


        [EFInclude]
        public Test Test { get; set; }

        [EFInclude]
        [IgnoreMap]
        public List<Answer> Answers { get; set; }

    }
}