﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AutoMapper;
using OnlineTesting.Attributes;

namespace OnlineTesting.Data.Models
{
    public class Test : BasicEntity
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public decimal Difficulty { get; set; }

        [Required]
        public string CategoryId { get; set; }


        [EFInclude]
        [IgnoreMap]
        public Category Category { get; set; }

        [EFInclude]
        [IgnoreMap]
        public List<Question> Questions { get; set; }

    }
}