﻿using System;
using System.ComponentModel.DataAnnotations;
using AutoMapper;
using OnlineTesting.Attributes;

namespace OnlineTesting.Data.Models
{
    public class TestResult : BasicEntity
    {
        [Required]
        public string TestId { get; set; }

        public DateTime? DatePassed { get; set; }

        [Required]
        public decimal AccumulatedScore { get; set; }

        [Required]
        public decimal MaxScore { get; set; }

        [Required]
        public bool IsRated { get; set; }

        public string ApplicationUserId { get; set; }

        [EFInclude]
        [IgnoreMap]
        public Test Test { get; set; }

        [EFInclude]
        [IgnoreMap]
        public ApplicationUser ApplicationUser { get; set; }
    }
}