﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using OnlineTesting.Data;
using OnlineTesting.Models;
using OnlineTesting.Models.AnswersViewModels;
using OnlineTesting.Models.TestViewModels;

namespace OnlineTesting.Interfaces
{
    public interface IAnswersService
    {
        IEnumerable<AnswerVM> GetAnswrsFor(string testId, string questionId, ApplicationUser applicationUser);
        void SaveAnswer(AnswerVM answer, ApplicationUser applicationUser);
        AnswerVM GetAnswer(string id, string questionid, string testId, ApplicationUser applicationUser);
        void RemoveAnswer(string id, string questionid, string testId, ApplicationUser applicationUser);
        bool CheckQuestionStateIsValid(string questionId, string testId, ApplicationUser applicationUser);
        IEnumerable<string> GetModelStateErrors(string testId, string questionId, ApplicationUser applicationUser);
    }
}