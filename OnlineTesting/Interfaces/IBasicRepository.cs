﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using OnlineTesting.Data.Models;

namespace OnlineTesting.Interfaces
{
    public interface IBasicRepository<T> where T: BasicEntity
    {
        T GetById(string id);
        IEnumerable<T> List();
        IEnumerable<T> ListAll();
        IEnumerable<T> List(Expression<Func<T, bool>> predicate);
        void Insert(T entity);
        void Delete(T entity);
        void ForceDelete(T entity);
        void Update(T entity);
        void AddOrUpdate(T entity);
        void AddOrUpdateRange(IEnumerable<T> entity);

    }
}
