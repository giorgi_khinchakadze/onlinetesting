﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OnlineTesting.Data;
using OnlineTesting.Models.HomeViewModels;
using OnlineTesting.Models.TestingViewModels;
using OnlineTesting.Models.TestViewModels;

namespace OnlineTesting.Interfaces
{
    public interface IHomeService
    {
        IEnumerable<TestVM> GetAvailableTests();
        TestVM GetTest(string id);
        IEnumerable<TestVM> GetAvailableTests(string name, string categoryId, int? difficulty);
        IEnumerable<TestResultVM> GetResultsFor(ApplicationUser applicationUser);
        IEnumerable<RatingVM> GetRatingsFor(string testId);
    }
}
