﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using OnlineTesting.Data;
using OnlineTesting.Models;
using OnlineTesting.Models.QuestionViewModels;
using OnlineTesting.Models.TestViewModels;

namespace OnlineTesting.Interfaces
{
    public interface IQuestionsService
    {
        bool AddOrUpdateQuestionOfCurrentTest(QuestionVM question, ApplicationUser applicationUser);
        IEnumerable<QuestionVM> GetCurrnentQuestions(string testId, ApplicationUser applicationUser);
        void RemoveQuestion(string testId, string id, ApplicationUser applicationUser);
        QuestionVM GetQuestion(string testId, string id, ApplicationUser applicationUser);
        bool CheckQuestionsAreValid(string testId, ApplicationUser applicationUser);
        IEnumerable<ValidationResult> GetQuestionErrors(string testId, ApplicationUser applicationUser);
    }
}
