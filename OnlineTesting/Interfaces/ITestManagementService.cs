﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using OnlineTesting.Data;
using OnlineTesting.Models;
using OnlineTesting.Models.TestViewModels;

namespace OnlineTesting.Interfaces
{
    public interface ITestManagementService
    {
        IEnumerable<TestVM> GetTests();
        IEnumerable<SelectListItem> GetSelectableCategories();
        void AddOrUpdateCachedTest(TestVM test, ApplicationUser user);
        TestVM GetCurrnentTest(string id, ApplicationUser user);
        bool CheckCurrentTestIsValid(string testId, ApplicationUser applicationUser);
        bool SaveCurrentTestToDatabase(string testId, ApplicationUser applicationUser);
        void DeactivateTest(string id);
        TestVM GetTestFromDataBase(string id);
        void SetCurrentCachedTest(TestVM test, ApplicationUser applicationUser);
        void CancelCurrentAction(string testId, ApplicationUser applicationUser);
    }
}
