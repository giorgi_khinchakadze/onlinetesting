﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OnlineTesting.Data;
using OnlineTesting.Models;
using OnlineTesting.Models.AnswersViewModels;
using OnlineTesting.Models.QuestionViewModels;
using OnlineTesting.Models.TestingViewModels;

namespace OnlineTesting.Interfaces
{
    public interface ITestingService
    {
        string PrepareForTesting(string id, bool isRated, ApplicationUser applicationUser);
        TestResultVM GetResult(string resultId, ApplicationUser applicationUser);
        void AddAnswerResult(List<string> answers, string questionId, string resultId, ApplicationUser applicationUser);
        QuestionVM GetNextQuestion(ApplicationUser applicationUser);
    }
}
