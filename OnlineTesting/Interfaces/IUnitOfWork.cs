﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using OnlineTesting.Data.Models;

namespace OnlineTesting.Interfaces
{
    public interface IUnitOfWork
    {
        IBasicRepository<Test> TestRepository { get; }
        IBasicRepository<Category> CategoryRepository { get; }
        IBasicRepository<Question> QuestionRepository { get; }
        IBasicRepository<Answer> AnswerRepository { get; }
        IBasicRepository<TestResult> ResultRepository { get; }

        void Save();
    }
}