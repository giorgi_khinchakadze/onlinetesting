﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using OnlineTesting.Models.UserViewModels;

namespace OnlineTesting.Interfaces
{
    public interface IUserManagementService
    {
        IEnumerable<UserVM> GetUserList();
        UserVM GetUser(string id);
        Task<IdentityResult> SaveUserEdit(UserVM user);
    }
}
