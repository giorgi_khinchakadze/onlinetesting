using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using OnlineTesting.Data.Models;

namespace OnlineTesting.Models.AnswersViewModels
{
    public class AnswerVM: BasicVM
    {
        [Required]
        [DisplayName("Text")]
        public string Text { get; set; }

        [DisplayName("IsCorrect")]
        public bool IsCorrect { get; set; }

        [HiddenInput]
        public string QuestionId { get; set; }
        [HiddenInput]
        public string TestId { get; set; }

        public static explicit operator AnswerVM(Answer a)
        {
            return AutoMapper.Mapper.Map<AnswerVM>(a);
        }

        public static explicit operator Answer(AnswerVM a)
        {
            return AutoMapper.Mapper.Map<Answer>(a);
        }


    }
}