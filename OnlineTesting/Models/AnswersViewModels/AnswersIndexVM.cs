﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OnlineTesting.Models.TestViewModels;

namespace OnlineTesting.Models.AnswersViewModels
{
    public class AnswersIndexVM
    {
        public IEnumerable<AnswerVM> Answers { get; set; }
        public string QuestionId { get; set; }
        public string TestId { get; set; }
    }
}