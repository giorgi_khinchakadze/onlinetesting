﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace OnlineTesting.Models
{
    public abstract class BasicVM
    {

        public BasicVM()
        {
            Id = Guid.NewGuid().ToString();
            IsActive = true;
        }
        
        [HiddenInput]
        public string Id { get; set; }
        
        [HiddenInput]
        public bool IsActive { get; set; }

    }
}
