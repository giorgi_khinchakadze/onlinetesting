﻿using System.ComponentModel;
using OnlineTesting.Data.Models;

namespace OnlineTesting.Models.CategoryViewModels
{
    public class CategoryVM : BasicVM
    {
        [DisplayName("Name")]
        public string Name { get; set; }


        public static explicit operator CategoryVM(Category a)
        {
            return AutoMapper.Mapper.Map<CategoryVM>(a);
        }

        public static explicit operator Category(CategoryVM a)
        {
            return AutoMapper.Mapper.Map<Category>(a);
        }
    }
}