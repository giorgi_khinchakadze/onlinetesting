﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using OnlineTesting.Models.UserViewModels;

namespace OnlineTesting.Models.HomeViewModels
{
    public class RatingVM
    {
        public UserVM User { get; set; }

        [DisplayName("Score")]
        public decimal Score { get; set; }
    }
}
