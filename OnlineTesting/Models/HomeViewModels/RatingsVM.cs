﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace OnlineTesting.Models.HomeViewModels
{
    public class RatingsVM
    {
        public IEnumerable<RatingVM> Ratings { get; set; }
        [DisplayName("Test")]
        public string TestId { get; set; }
    }
}