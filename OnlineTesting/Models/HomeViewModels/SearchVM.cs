﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using OnlineTesting.Models.TestViewModels;

namespace OnlineTesting.Models.HomeViewModels
{
    public class SearchVM
    {
        [DisplayName("Category")]
        public string CategoryId { get; set; }

        [DisplayName("Name")]
        public string Name { get; set; }

        [DisplayName("Difficulty")]
        public int? Difficulty { get; set; }
        
        public IEnumerable<TestVM> Tests { get; set; }
    }
}
