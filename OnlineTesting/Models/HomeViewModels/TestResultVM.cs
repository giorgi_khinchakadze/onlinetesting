﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using OnlineTesting.Data;
using OnlineTesting.Data.Models;
using OnlineTesting.Models.TestViewModels;

namespace OnlineTesting.Models.TestingViewModels
{
    public class TestResultVM
    {
        [DisplayName("Test")]
        public string TestId { get; set; }

        [DisplayName("DatePassed")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? DatePassed { get; set; }

        [DisplayName("AccumulatedScore")]
        public decimal AccumulatedScore { get; set; }

        [DisplayName("MaxScore")]
        public decimal MaxScore { get; set; }


        [DisplayName("IsRated")]
        public bool IsRated { get; set; }

        public string ApplicationUserId { get; set; }

        public TestVM Test { get; set; }
        
        public ApplicationUser ApplicationUser { get; set; }

        public static explicit operator TestResultVM(TestResult a)
        {
            return AutoMapper.Mapper.Map<TestResultVM>(a);
        }

        public static explicit operator TestResult(TestResultVM a)
        {
            return AutoMapper.Mapper.Map<TestResult>(a);
        }
    }
}