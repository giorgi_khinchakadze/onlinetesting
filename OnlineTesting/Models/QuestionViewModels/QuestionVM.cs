using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using OnlineTesting.Data.Models;
using OnlineTesting.Models.AnswersViewModels;

namespace OnlineTesting.Models.QuestionViewModels
{
    public class QuestionVM : BasicVM
    {
        public QuestionVM()
        {
            Answers = new List<AnswerVM>();
        }

        [Required]
        [Range(10, 600)]
        [DisplayName("Time")]
        public int Time { get; set; }

        [Range(1, 5)]
        [Required]
        [DisplayName("Score")]
        public int Score { get; set; }

        [Required]
        [DisplayName("Text")]
        public string Text { get; set; }

        [HiddenInput]
        public string TestId { get; set; }

        public List<AnswerVM> Answers { get; set; }


        public static explicit operator QuestionVM(Question a)
        {
            return AutoMapper.Mapper.Map<QuestionVM>(a);
        }

        public static explicit operator Question(QuestionVM a)
        {
            return AutoMapper.Mapper.Map<Question>(a);
        }


        public IEnumerable<string> ValidateAnswers(IStringLocalizerFactory factory)
        {
            var localizer = factory.Create(typeof(QuestionVM));


            if (!IsActive)
            {
                yield break;
            }
            var hasFalse = false;
            var hasTrue = false;
            foreach (var answerVm in Answers.Where(ans => ans.IsActive))
            {
                if (answerVm.IsCorrect)
                {
                    hasTrue = true;
                }
                else
                {
                    hasFalse = true;
                }
            }

            if (!hasTrue)
            {
                yield return localizer["QuestionRequiresTrue"];
            }
            if (!hasFalse)
            {
                yield return localizer["QuestionRequiresFalse"];
            }
        }
    }
}