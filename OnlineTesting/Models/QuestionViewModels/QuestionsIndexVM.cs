﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace OnlineTesting.Models.QuestionViewModels
{
    public class QuestionsIndexVM
    {
        public IEnumerable<QuestionVM> Questions { get; set; }
        [HiddenInput]
        public string TestId { get; set; }
    }
}
