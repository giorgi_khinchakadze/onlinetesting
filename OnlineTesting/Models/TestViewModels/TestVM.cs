﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.Extensions.Localization;
using OnlineTesting.Data.Models;
using OnlineTesting.Models.CategoryViewModels;
using OnlineTesting.Models.QuestionViewModels;

namespace OnlineTesting.Models.TestViewModels
{
    public class TestVM : BasicVM
    {
        public TestVM()
        {
            Questions = new List<QuestionVM>();
        }

        [Required]
        [DisplayName("Name")]
        public string Name { get; set; }

        [Required]
        [DisplayName("Category")]
        public string CategoryId { get; set; }

        [DisplayName("Category")]
        public CategoryVM Category { get; set; }

        [Required]
        [Range(1, 10)]
        [DisplayName("Difficulty")]
        public int Difficulty { get; set; }

        public List<QuestionVM> Questions { get; set; }

        public static explicit operator TestVM(Test a)
        {
            return AutoMapper.Mapper.Map<TestVM>(a);
        }

        public static explicit operator Test(TestVM a)
        {
            return AutoMapper.Mapper.Map<Test>(a);
        }


        public IEnumerable<ValidationResult> ValidateQuestions(IStringLocalizerFactory factory)
        {
            var localizer = factory.Create(typeof(TestVM));

            if (!Questions.Any(q => q.IsActive))
            {
                yield return new ValidationResult(localizer["TestNeedAtLeastOneQuestion"]);
            }

            foreach (var question in Questions)
            {
                if (question.ValidateAnswers(factory).Any())
                {
                    yield return new ValidationResult(localizer["QuestionsInvalid"], new[] {question.Id});
                }
            }
        }
    }
}