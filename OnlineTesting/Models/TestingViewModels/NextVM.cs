﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OnlineTesting.Models.QuestionViewModels;

namespace OnlineTesting.Models.TestingViewModels
{
    public class NextVM
    {
        public QuestionVM Question { get; set; }
        [HiddenInput]
        public string ResultId { get; set; }
    }
}
