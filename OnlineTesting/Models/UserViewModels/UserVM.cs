﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc;
using OnlineTesting.Data;

namespace OnlineTesting.Models.UserViewModels
{
    public class UserVM
    {
        
        [HiddenInput]
        public string Id { get; set; }
        
        [Required]
        [DisplayName("Username")]
        public string Username { get; set; }

        [Required]
        [DisplayName("Email")]
        [EmailAddress]
        public string Email { get; set; }

        //[DataType(DataType.Password)]
        //public string Password { get; set; }
        
        [Required]
        [DisplayName("IsActive")]
        public bool IsActive { get; set; }

        public static explicit operator UserVM(ApplicationUser a)
        {
            return AutoMapper.Mapper.Map<UserVM>(a);
        }

        public static explicit operator ApplicationUser(UserVM a)
        {
            return AutoMapper.Mapper.Map<ApplicationUser>(a);
        }


    }
}
