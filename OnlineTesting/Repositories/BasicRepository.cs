﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using OnlineTesting.Attributes;
using OnlineTesting.Data;
using OnlineTesting.Data.Models;
using OnlineTesting.Interfaces;

namespace OnlineTesting.Repositories
{
    //TODO look over
    public class BasicRepository<T> : IBasicRepository<T> where T : BasicEntity
    {
        private readonly ApplicationDbContext _ctx;

        private List<string> _efIncludeTree;

        private List<string> IncludeTree
        {
            get { return _efIncludeTree = _efIncludeTree ?? GetEFIncludeTree().ToList(); }
        }

        public BasicRepository(ApplicationDbContext ctx)
        {
            _ctx = ctx;
        }

        private IEnumerable<string> GetEFIncludeTree(Type type = null, List<Type> except = null)
        {
            except = except ?? new List<Type>();
            type = type ?? typeof(T);

            var properties = type.GetProperties()
                .Where(info =>
                    info.CustomAttributes.Any(data =>
                        data.AttributeType == typeof(EFIncludeAttribute)))
                .Select(info =>
                {
                    if (!info.PropertyType.GetGenericArguments().Any())
                    {
                        return new {Type = info.PropertyType, Name = info.Name};
                    }
                    return new {Type = info.PropertyType.GetGenericArguments().First(), Name = info.Name};
                }).Where(obj => !except.Contains(obj.Type));

            except.Add(type);
            foreach (var prop in properties)
            {
                yield return prop.Name;
                var children = GetEFIncludeTree(prop.Type, except);
                foreach (var child in children)
                {
                    yield return $"{prop.Name}.{child}";
                }
            }
            except.Remove(type);
        }

        private IQueryable<T> Set()
        {
            var query = _ctx.Set<T>().AsQueryable();

            foreach (var node in IncludeTree)
            {
                query = query.Include(node);
            }

            return query.OrderBy(arg => arg.Id);
        }

        public virtual T GetById(string id)
        {
            return Set().FirstOrDefault(arg => arg.Id == id);
        }


        public virtual IEnumerable<T> List()
        {
            return Set().Where(d => d.IsActive).AsEnumerable();
        }

        public virtual IEnumerable<T> List(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            return Set()
                .Where(d => d.IsActive)
                .Where(predicate)
                .AsEnumerable();
        }

        public virtual IEnumerable<T> ListAll()
        {
            return Set().AsEnumerable();
        }

        public void Insert(T entity)
        {
            entity.IsActive = true;
            entity.DateCreated = DateTime.Now;
            _ctx.Set<T>().Add(entity);
        }

        public void Update(T entity)
        {
            _ctx.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(T entity)
        {
            entity.IsActive = false;
            entity.DateDeleted = DateTime.Now;
            _ctx.Entry(entity).State = EntityState.Modified;
        }

        public void ForceDelete(T entity)
        {
            _ctx.Remove(entity);
        }


        public void AddOrUpdate(T entity)
        {
            var e = _ctx.Set<T>().AsNoTracking().FirstOrDefault(arg => arg.Id == entity.Id);
            if (e != null)
            {
                if (entity.IsActive == false)
                {
                    Delete(entity);
                }
                else
                {
                    Update(entity);
                }
                return;
            }

            if (entity.IsActive)
            {
                Insert(entity);
            }
        }

        public void AddOrUpdateRange(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                AddOrUpdate(entity);
            }
        }
    }
}