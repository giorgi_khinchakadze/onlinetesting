﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using OnlineTesting.Data;
using OnlineTesting.Data.Models;
using OnlineTesting.Models;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using OnlineTesting.Interfaces;
using OnlineTesting.Util;

namespace OnlineTesting
{
    public static class SeedData
    {
        public static async Task Initialize(IApplicationBuilder app)
        {
            InitializeDatabase(app);
            await InitializeBasicUser(app);
            InitializeCategories(app);
        }

        private static void InitializeDatabase(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var applicationDbContext =
                    serviceScope.ServiceProvider
                        .GetService<ApplicationDbContext>() as ApplicationDbContext;

                applicationDbContext.Database.Migrate();

            }

        }


        private static void InitializeCategories(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var repo =
                    serviceScope.ServiceProvider.GetService<IUnitOfWork>() as IUnitOfWork;
                repo.CategoryRepository.AddOrUpdateRange(
                    new[]
                    {
                        new Category()
                        {
                            Id = "6c15e7fb-a74b-47c5-a464-17b091dbca4c",
                            Name = "Mathematics",
                            IsActive = true
                        },
                        new Category()
                        {
                            Id = "2c82bcab-ab73-423d-9cda-6f4f1c9d1a3f",
                            Name = "Computer Science",
                            IsActive = true
                        },
                        new Category()
                        {
                            Id = "1293bc87-35c8-491b-9e51-42db58aad846",
                            Name = "Other",
                            IsActive = true
                        },
                    });
                repo.Save();
            }
        }

        private static async Task InitializeBasicUser(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var userManager =
                    serviceScope.ServiceProvider
                        .GetService<UserManager<ApplicationUser>>() as UserManager<ApplicationUser>;
                var roleManager =
                    serviceScope.ServiceProvider.GetService<RoleManager<IdentityRole>>() as RoleManager<IdentityRole>;

                var user = await userManager.FindByNameAsync("admin");
                if (user == null)
                {
                    await roleManager.CreateAsync(new IdentityRole("admin"));
                    await roleManager.CreateAsync(new IdentityRole("user"));

                    user = new ApplicationUser() {UserName = "admin", Email = "admin@admin.com"};
                    await userManager.CreateAsync(user, "admin");
                    await userManager.AddToRoleAsync(user, "admin");
                }
            }
        }
    }
}