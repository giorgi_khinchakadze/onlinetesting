﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using OnlineTesting.Contstants;
using OnlineTesting.Data;
using OnlineTesting.Interfaces;
using OnlineTesting.Models;
using OnlineTesting.Models.AnswersViewModels;
using OnlineTesting.Models.TestViewModels;

namespace OnlineTesting.Services
{
    public class AnswersService : BasicService, IAnswersService
    {
        private readonly ITestManagementService _testManagementService;
        private readonly IStringLocalizerFactory _factory;

        public AnswersService(
            IMemoryCache memoryCache,
            ITestManagementService testManagementService,
            IStringLocalizerFactory factory
        ) : base(memoryCache)
        {
            _testManagementService = testManagementService;
            _factory = factory;
        }


        public IEnumerable<AnswerVM> GetAnswrsFor(string testId, string questionId, ApplicationUser applicationUser)
        {
            var test = _testManagementService.GetCurrnentTest(testId, applicationUser);
            return test?.Questions.Find(q => q.Id == questionId).Answers.Where(a => a.IsActive).OrderBy(a => a.Id);
        }

        public void SaveAnswer(AnswerVM answer, ApplicationUser applicationUser)
        {
            var test = _testManagementService.GetCurrnentTest(answer.TestId, applicationUser);
            var question = test?.Questions.FirstOrDefault(q => q.Id == answer.QuestionId);
            var ans = question?.Answers.FirstOrDefault(a => a.Id == answer.Id);
            question?.Answers.Remove(ans);
            question?.Answers.Add(answer);
        }

        public AnswerVM GetAnswer(string id, string questionId, string testId, ApplicationUser applicationUser)
        {
            var test = _testManagementService.GetCurrnentTest(testId, applicationUser);
            return test?.Questions.FirstOrDefault(q => q.Id == questionId)?.Answers.FirstOrDefault(a => a.Id == id);
        }

        public void RemoveAnswer(string answerId, string questionId, string testId, ApplicationUser applicationUser)
        {
            var test = _testManagementService.GetCurrnentTest(testId, applicationUser);
            var question = test?.Questions.FirstOrDefault(q => q.Id == questionId);
            var ans = question?.Answers.FirstOrDefault(a => a.Id == answerId);
            if (ans != null)
            {
                ans.IsActive = false;
            }
        }

        public bool CheckQuestionStateIsValid(string questionId, string testId, ApplicationUser applicationUser)
        {
            return !(GetModelStateErrors(testId, questionId, applicationUser)?.Any() ?? false);
        }

        public IEnumerable<string> GetModelStateErrors(string testId, string questionId,
            ApplicationUser applicationUser)
        {
            var test = _testManagementService.GetCurrnentTest(testId, applicationUser);
            var question = test?.Questions.FirstOrDefault(q => q.Id == questionId);
            return question?.ValidateAnswers(_factory);
        }
    }
}