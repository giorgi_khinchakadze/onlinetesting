﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using OnlineTesting.Contstants;
using OnlineTesting.Data;
using OnlineTesting.Models;
using OnlineTesting.Models.TestViewModels;

namespace OnlineTesting.Services
{
    public class BasicService
    {
        private readonly IMemoryCache _memoryCache;

        protected BasicService(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }

        protected void AddToCache(string key, object obj, ApplicationUser user, TimeSpan? absolute = null,
            TimeSpan? sliding = null, PostEvictionDelegate callback = null)
        {
            var fullKey = $"{key}_{user.Id}";
            var options = new MemoryCacheEntryOptions();
            if (callback != null)
            {
                options.RegisterPostEvictionCallback(callback);
            }

            sliding = sliding ?? TimeSpan.FromMinutes(30);


            if (absolute != null)
            {
                options.SetAbsoluteExpiration(absolute.Value);
            }
            else
            {
                options.SetSlidingExpiration(sliding.Value);
            }

            _memoryCache.Set(fullKey, obj, options);
        }

        protected object GetFromCache(string key, ApplicationUser user)
        {
            var fullKey = $"{key}_{user.Id}";
            return _memoryCache.Get(fullKey);
        }

        protected void RemoveFromCache(string key, ApplicationUser user)
        {
            var fullKey = $"{key}_{user.Id}";
            _memoryCache.Remove(fullKey);
        }
    }
}