﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using OnlineTesting.Data;
using OnlineTesting.Interfaces;
using OnlineTesting.Models.HomeViewModels;
using OnlineTesting.Models.TestingViewModels;
using OnlineTesting.Models.TestViewModels;
using OnlineTesting.Models.UserViewModels;

namespace OnlineTesting.Services
{
    public class HomeService : BasicService, IHomeService
    {
        private readonly ITestManagementService _testManagementService;
        private readonly IUnitOfWork _unitOfWork;

        public HomeService(
            IMemoryCache memoryCache,
            ITestManagementService testManagementService,
            IUnitOfWork unitOfWork
        ) : base(memoryCache)
        {
            _testManagementService = testManagementService;
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<TestVM> GetAvailableTests()
        {
            return _testManagementService.GetTests();
        }

        public TestVM GetTest(string id)
        {
            return _testManagementService.GetTestFromDataBase(id);
        }

        public IEnumerable<TestVM> GetAvailableTests(string name, string categoryId, int? difficulty)
        {
            var tests = _testManagementService.GetTests();
            if (name != null)
            {
                tests = tests.Where(test => test.Name.ToLowerInvariant().Contains(name.ToLowerInvariant()));
            }
            if (categoryId != null)
            {
                tests = tests.Where(test => test.CategoryId == categoryId);
            }
            if (difficulty != null)
            {
                tests = tests.Where(test => test.Difficulty == difficulty);
            }

            return tests;
        }

        public IEnumerable<TestResultVM> GetResultsFor(ApplicationUser applicationUser)
        {
            return
                _unitOfWork.ResultRepository.List().Where(result => result.ApplicationUserId == applicationUser.Id)
                    .Select(result => (TestResultVM) result);
        }

        public IEnumerable<RatingVM> GetRatingsFor(string testId)
        {
            var results = _unitOfWork.ResultRepository.List()
                .Where(result => result.IsRated && (testId == null || result.TestId == testId))
                .GroupBy(result => result.ApplicationUser).Select(
                    grouping => new RatingVM()
                    {
                        User = (UserVM) grouping.Key,
                        Score =
                            grouping
                                .GroupBy(result => result.Test)
                                .Select(testResults =>
                                    testResults.OrderByDescending(result => result.DatePassed).First().AccumulatedScore)
                                .Sum()
                    })
                .OrderByDescending(vm => vm.Score);

            return results;
        }
    }
}