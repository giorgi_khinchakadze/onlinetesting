﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using OnlineTesting.Contstants;
using OnlineTesting.Data;
using OnlineTesting.Interfaces;
using OnlineTesting.Models;
using OnlineTesting.Models.QuestionViewModels;
using OnlineTesting.Models.TestViewModels;

namespace OnlineTesting.Services
{
    public class QuestionsService : BasicService, IQuestionsService
    {
        private readonly IStringLocalizerFactory _factory;
        private readonly ITestManagementService _testManagementService;

        public QuestionsService(
            IMemoryCache memoryCache,
            IStringLocalizerFactory factory,
            ITestManagementService testManagementService
        ) : base(memoryCache)
        {
            _factory = factory;
            _testManagementService = testManagementService;
        }

        public bool AddOrUpdateQuestionOfCurrentTest(QuestionVM question, ApplicationUser applicationUser)
        {
            var test = _testManagementService.GetCurrnentTest(question.TestId, applicationUser);
            if (test == null)
            {
                return false;
            }

            var old = test.Questions.FirstOrDefault(q => q.Id == question.Id);
            if (old != null)
            {
                test.Questions.Remove(old);
                question.Answers = old.Answers;
            }
            question.TestId = test.Id;
            test.Questions.Add(question);

            return true;
        }

        public IEnumerable<QuestionVM> GetCurrnentQuestions(string testId, ApplicationUser applicationUser)
        {
            var test = _testManagementService.GetCurrnentTest(testId, applicationUser);
            return test?.Questions.Where(q => q.IsActive).OrderBy(q => q.Id);
        }

        public void RemoveQuestion(string testId, string id, ApplicationUser applicationUser)
        {
            var test = _testManagementService.GetCurrnentTest(testId, applicationUser);
            var question = test?.Questions.FirstOrDefault(q => q.Id == id);
            if (question != null)
            {
                question.IsActive = false;
            }
        }

        public QuestionVM GetQuestion(string testId, string id, ApplicationUser applicationUser)
        {
            var test = _testManagementService.GetCurrnentTest(testId, applicationUser);
            return test?.Questions.FirstOrDefault(q => q.Id == id && q.IsActive);
        }

        public bool CheckQuestionsAreValid(string testId, ApplicationUser applicationUser)
        {
            return !GetQuestionErrors(testId, applicationUser).Any();
        }

        public IEnumerable<ValidationResult> GetQuestionErrors(string testId, ApplicationUser applicationUser)
        {
            var test = _testManagementService.GetCurrnentTest(testId, applicationUser);
            return test?.ValidateQuestions(_factory);
        }
    }
}