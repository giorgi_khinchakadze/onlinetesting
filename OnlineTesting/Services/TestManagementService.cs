﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Localization;
using OnlineTesting.Contstants;
using OnlineTesting.Data;
using OnlineTesting.Data.Models;
using OnlineTesting.Interfaces;
using OnlineTesting.Models;
using OnlineTesting.Models.TestViewModels;

namespace OnlineTesting.Services
{
    internal class TestManagementService : BasicService, ITestManagementService
    {
        private readonly IStringLocalizerFactory _factory;
        private readonly IUnitOfWork _unitOfWork;

        public TestManagementService(
            IMemoryCache memoryCache,
            IStringLocalizerFactory factory,
            IUnitOfWork unitOfWork
        ) : base(memoryCache)
        {
            _factory = factory;
            _unitOfWork = unitOfWork;
        }


        public IEnumerable<TestVM> GetTests()
        {
            return _unitOfWork.TestRepository.List().Select(test => (TestVM) test);
        }

        public IEnumerable<SelectListItem> GetSelectableCategories()
        {
            return _unitOfWork.CategoryRepository.List()
                .Select(c => new SelectListItem() {Text = c.Name, Value = c.Id.ToString()});
        }

        public void AddOrUpdateCachedTest(TestVM test, ApplicationUser user)
        {
            var old = GetCurrnentTest(test.Id, user);
            if (old != null && old.Id == test.Id)
            {
                old.CategoryId = test.CategoryId;
                old.Difficulty = test.Difficulty;
                old.Name = test.Name;
                return;
            }
            AddToCache(test.Id, test, user);
        }

        public TestVM GetCurrnentTest(string id, ApplicationUser user)
        {
            return GetFromCache(id, user) as TestVM;
        }

        public bool CheckCurrentTestIsValid(string testId, ApplicationUser applicationUser)
        {
            var test = GetCurrnentTest(testId, applicationUser);

            return !(test?.ValidateQuestions(_factory).Any() ?? false);
        }

        public bool SaveCurrentTestToDatabase(string testId, ApplicationUser applicationUser)
        {
            var test = GetCurrnentTest(testId, applicationUser);
            if (test == null)
            {
                return false;
            }

            var testEntity = (Test) test;
            var questionEntities = test.Questions.Select(vm => (Question) vm);
            var answerEntities = test.Questions.SelectMany(vm => vm.Answers.Select(ans => (Answer) ans));

            _unitOfWork.TestRepository.AddOrUpdate(testEntity);
            _unitOfWork.QuestionRepository.AddOrUpdateRange(questionEntities);
            _unitOfWork.AnswerRepository.AddOrUpdateRange(answerEntities);
            _unitOfWork.Save();

            RemoveFromCache(test.Id, applicationUser);
            return true;
        }

        public void DeactivateTest(string id)
        {
            _unitOfWork.TestRepository.Delete(new Test() {Id = id});
            _unitOfWork.Save();
        }

        public TestVM GetTestFromDataBase(string id)
        {
            return (TestVM) _unitOfWork.TestRepository.GetById(id);
        }

        public void SetCurrentCachedTest(TestVM test, ApplicationUser applicationUser)
        {
            AddToCache(test.Id, test, applicationUser);
        }

        public void CancelCurrentAction(string testId, ApplicationUser applicationUser)
        {
            RemoveFromCache(testId, applicationUser);
        }
    }
}