﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyModel;
using Microsoft.Extensions.ProjectModel.Resolution;
using OnlineTesting.Data;
using OnlineTesting.Data.Models;
using OnlineTesting.Extensions;
using OnlineTesting.Interfaces;
using OnlineTesting.Models;
using OnlineTesting.Models.AnswersViewModels;
using OnlineTesting.Models.QuestionViewModels;
using OnlineTesting.Models.TestingViewModels;
using OnlineTesting.Models.TestViewModels;
using OnlineTesting.Repositories;

namespace OnlineTesting.Services
{
    public class TestingService : BasicService, ITestingService
    {
        private const string CurrentTest = "current_test";
        private const string CurrentTestResult = "current_test_result";

        private readonly IUnitOfWork _unitOfWork;


        //TODO save result to db every time
        public TestingService(
            IMemoryCache memoryCache,
            IUnitOfWork unitOfWork
        ) : base(memoryCache)
        {
            _unitOfWork = unitOfWork;
        }


        public string PrepareForTesting(string id, bool isRated, ApplicationUser applicationUser)
        {
            var test = (TestVM) _unitOfWork.TestRepository.GetById(id);

            test.Questions.Shuffle();
            test.Questions.ForEach(question => question.Answers.Shuffle());

            AddToCache(CurrentTest, test, applicationUser,
                TimeSpan.FromSeconds(test.Questions.Sum(question => question.Time)));


            var result = new TestResult()
            {
                AccumulatedScore = 0,
                DatePassed = DateTime.Now,
                MaxScore = test.Questions.Sum(question => question.Score),
                TestId = test.Id,
                IsRated = isRated,
                ApplicationUserId = applicationUser.Id,
            };

            _unitOfWork.ResultRepository.Insert(result);
            _unitOfWork.Save();

            return result.Id;
        }

        public TestResultVM GetResult(string resultId, ApplicationUser applicationUser)
        {
            var result = (TestResultVM) _unitOfWork.ResultRepository.GetById(resultId);

            return result;
        }

        public void AddAnswerResult(List<string> answers, string questionId, string resultId,
            ApplicationUser applicationUser)
        {
            var test = GetFromCache(CurrentTest, applicationUser) as TestVM;
            var question = test?.Questions.FirstOrDefault(vm => vm.Id == questionId);
            if (question == null)
            {
                return;
            }
            test.Questions.Remove(question);


            if (question.Answers.Any(vm =>
                !vm.IsCorrect && answers.Contains(vm.Id) ||
                vm.IsCorrect && !answers.Contains(vm.Id))
            )
            {
                return;
            }

            var result = _unitOfWork.ResultRepository.GetById(resultId);
            result.AccumulatedScore += question.Score;
            _unitOfWork.Save();
        }

        public QuestionVM GetNextQuestion(ApplicationUser applicationUser)
        {
            var test = GetFromCache(CurrentTest, applicationUser) as TestVM;

            return test?.Questions.FirstOrDefault();
        }
    }
}