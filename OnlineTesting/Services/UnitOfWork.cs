﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime;
using System.Threading.Tasks;
using OnlineTesting.Data;
using OnlineTesting.Data.Models;
using OnlineTesting.Interfaces;
using OnlineTesting.Repositories;

namespace OnlineTesting.Services
{
    public sealed class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly ApplicationDbContext _ctx;

        private IBasicRepository<Test> _testRepository;
        private IBasicRepository<Category> _categoryRepository;
        private IBasicRepository<Question> _questionRepository;
        private IBasicRepository<Answer> _answerRepository;
        private IBasicRepository<TestResult> _resultRepository;

        public UnitOfWork(ApplicationDbContext ctx)
        {
            _ctx = ctx;
        }

        public void Dispose()
        {
            _ctx?.Dispose();
        }

        public IBasicRepository<Test> TestRepository =>
            _testRepository = _testRepository ?? new BasicRepository<Test>(_ctx);

        public IBasicRepository<Category> CategoryRepository => _categoryRepository =
            _categoryRepository ?? new BasicRepository<Category>(_ctx);

        public IBasicRepository<Question> QuestionRepository => _questionRepository =
            _questionRepository ?? new BasicRepository<Question>(_ctx);

        public IBasicRepository<Answer> AnswerRepository =>
            _answerRepository = _answerRepository ?? new BasicRepository<Answer>(_ctx);

        public IBasicRepository<TestResult> ResultRepository =>
            _resultRepository = _resultRepository ?? new BasicRepository<TestResult>(_ctx);

        public void Save()
        {
            _ctx.SaveChanges();
        }
    }
}