﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using OnlineTesting.Data;
using OnlineTesting.Interfaces;
using OnlineTesting.Models;
using OnlineTesting.Models.UserViewModels;

namespace OnlineTesting.Services
{
    public class UserManagementService : IUserManagementService
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public UserManagementService(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        public IEnumerable<UserVM> GetUserList()
        {
            return _userManager.Users.ToList().Select(user => (UserVM) user);
        }

        public UserVM GetUser(string id)
        {
            return (UserVM) _userManager.Users.FirstOrDefault(u => u.Id == id);
        }

        public async Task<IdentityResult> SaveUserEdit(UserVM user)
        {
            var old = _userManager.Users.FirstOrDefault(u => u.Id == user.Id);
            if (old != null)
            {
                old.UserName = user.Username;
                old.Email = user.Email;
                if (user.IsActive)
                {
                    old.LockoutEnabled = false;
                    old.LockoutEnd = null;
                }
                else
                {
                    old.LockoutEnabled = true;
                    old.LockoutEnd = DateTimeOffset.MaxValue;
                }

                return await _userManager.UpdateAsync(old);
            }
            return IdentityResult.Failed();
        }
    }
}