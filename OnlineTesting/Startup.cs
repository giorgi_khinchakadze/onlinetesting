﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using OnlineTesting.Data;
using OnlineTesting.Data.Models;
using OnlineTesting.Interfaces;
using OnlineTesting.Models;
using OnlineTesting.Models.AnswersViewModels;
using OnlineTesting.Models.CategoryViewModels;
using OnlineTesting.Models.QuestionViewModels;
using OnlineTesting.Models.TestingViewModels;
using OnlineTesting.Models.TestViewModels;
using OnlineTesting.Models.UserViewModels;
using OnlineTesting.Repositories;
using OnlineTesting.Services;
using OnlineTesting.Validation;

namespace OnlineTesting
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", true, true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true);


            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
            ConfigureAutoMapper();
        }


        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            ConfigureDependencyInjection(services);


            // Add framework services.

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));


            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 5;

                options.User.RequireUniqueEmail = false;
            });


            services.AddLocalization(options => options.ResourcesPath = "Resources");

            services.AddMvc(options =>
                {
                    var localizer = services.BuildServiceProvider().GetService<IStringLocalizer<ValidationResources>>();
                    options.ModelMetadataDetailsProviders.Add(new ValidationMetadataProvider(localizer));
                    options.ModelBindingMessageProvider.ValueIsInvalidAccessor =
                        (x) => localizer["ValueIsInvalidAccessor"];
                    options.ModelBindingMessageProvider.ValueMustBeANumberAccessor =
                        (x) => localizer["ValueMustBeANumberAccessor"];
                    options.ModelBindingMessageProvider.MissingBindRequiredValueAccessor =
                        (x) => localizer["MissingBindRequiredValueAccessor", x];
                    options.ModelBindingMessageProvider.AttemptedValueIsInvalidAccessor =
                        (x, y) => localizer["AttemptedValueIsInvalidAccessor", x, y];
                    options.ModelBindingMessageProvider.MissingKeyOrValueAccessor =
                        () => localizer["MissingKeyOrValueAccessor"];
                    options.ModelBindingMessageProvider.UnknownValueIsInvalidAccessor =
                        (x) => localizer["UnknownValueIsInvalidAccessor", x];
                    options.ModelBindingMessageProvider.ValueMustNotBeNullAccessor =
                        (x) => localizer["ValueMustNotBeNullAccessor", x];
                })
                .AddDataAnnotationsLocalization(options =>
                {
                    options.DataAnnotationLocalizerProvider =
                        (type, factory) => factory.Create(typeof(SharedResources));
                })
                .AddViewLocalization();


            services.AddMemoryCache();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();


            var supportedCultures = new[]
            {
                new CultureInfo("ka-GE")
            };
            app.UseRequestLocalization(new RequestLocalizationOptions()
            {
                DefaultRequestCulture = new RequestCulture("ka-GE"),
                SupportedCultures = supportedCultures,
                SupportedUICultures = supportedCultures
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Error/Index");
            }

            app.UseStaticFiles();

            app.UseIdentity();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    "default",
                    "{controller=Home}/{action=Index}/{id?}");
            });

            SeedData.Initialize(app).Wait();
        }


        private void ConfigureDependencyInjection(IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<ITestManagementService, TestManagementService>();
            services.AddScoped<IAnswersService, AnswersService>();
            services.AddScoped<IQuestionsService, QuestionsService>();
            services.AddScoped<IUserManagementService, UserManagementService>();
            services.AddScoped<IHomeService, HomeService>();
            services.AddScoped<ITestingService, TestingService>();
        }

        private void ConfigureAutoMapper()
        {
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Test, TestVM>()
                    .ForMember(vm => vm.Questions,
                        opt => opt.ResolveUsing(test => test.Questions.Where(question => question.IsActive)));
                cfg.CreateMap<TestVM, Test>();

                cfg.CreateMap<Question, QuestionVM>()
                    .ForMember(vm => vm.Answers,
                        opt => { opt.ResolveUsing(question => question.Answers.Where(answer => answer.IsActive)); });
                cfg.CreateMap<QuestionVM, Question>();

                cfg.CreateMap<Answer, AnswerVM>();
                cfg.CreateMap<AnswerVM, Answer>();

                cfg.CreateMap<Category, CategoryVM>();
                cfg.CreateMap<CategoryVM, Category>();

                cfg.CreateMap<TestResult, TestResultVM>();
                cfg.CreateMap<TestResultVM, TestResult>();

                cfg.CreateMap<ApplicationUser, UserVM>()
                    .ForMember(vm => vm.IsActive,
                        opt => opt.ResolveUsing(user => !user.LockoutEnabled || user.LockoutEnd == null));
            });
        }
    }
}