﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Cryptography;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using OnlineTesting.Data.Models;

namespace OnlineTesting.Extensions
{
    public static class ListExtensions
    {

        public static void Shuffle<T>(this IList<T> list)
        {
            using (var provider = RandomNumberGenerator.Create())
            {
                var n = list.Count;
                while (n > 1)
                {
                    var box = new byte[1];
                    do
                    {
                        provider.GetBytes(box);
                    } while (!(box[0] < n * (byte.MaxValue / n)));
                    var k = box[0] % n;
                    n--;
                    var value = list[k];
                    list[k] = list[n];
                    list[n] = value;
                }
            }
        }
    }
}
