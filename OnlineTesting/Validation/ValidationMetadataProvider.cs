﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding.Metadata;
using Microsoft.Extensions.Localization;

namespace OnlineTesting.Validation
{
    public class ValidationMetadataProvider : IValidationMetadataProvider
    {
        private readonly IStringLocalizer _localizer;

        public ValidationMetadataProvider(IStringLocalizer localizer)
        {
            _localizer = localizer;
        }

        public void CreateValidationMetadata(
            ValidationMetadataProviderContext context)
        {
            foreach (var attribute in context.ValidationMetadata.ValidatorMetadata)
            {
                if (attribute is ValidationAttribute tAttr)
                {
                    tAttr.ErrorMessage = _localizer[tAttr.GetType().Name];
                }
            }
        }
    }
}